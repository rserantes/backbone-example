//Espacio de nombres:
//Creamos una variable local en la incialización
//De este modo si en algún momento cambiamos el nombre TODOApp, solo se cambia una línea
//y no todo el cuerpo de esta función de inicialización
(function(app) {
	app.router = new app.Router();
})(window.TODOApp);

